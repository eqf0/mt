import
  std/[
    terminal,
    strutils,
    strscans,
  ],
  cutelog

type
  LineStyle* = object
    style: set[Style]
    fg: ForegroundColor
  StyledLine* = object
    str*: string
    sty*: LineStyle
  StyledText* = object
    lines*: seq[StyledLine]
    colored*: bool
  MdStyle* = object
    title, summary, bullet, code: LineStyle
  StrStyle* = (LineStyle | MdStyle | CutePalette)

const 
  defaultMdSty = MdStyle(
    title: LineStyle(
      style: {styleBright, styleUnderscore},
      fg: fgMagenta,
    ),
    summary: LineStyle(
      style: {styleDim, styleItalic},
      fg: fgGreen,
    ),
    bullet: LineStyle(
      style: {styleItalic},
      fg: fgBlue,
    ),
    code: LineStyle(
      style: {styleBright},
      fg: fgDefault,
    ),
  )

  helpSty* = LineStyle(
    style: {styleBright, styleItalic},
    fg: fgDefault
  )

  listCmdSty* = LineStyle(
    style: {styleItalic},
    fg: fgGreen
  )

proc `colored=`*(s: var StyledText, b: bool) =
  s.colored = b

proc `$`*(l: CutePalette | LineStyle): string =
  for st in l.style:
    result.add st.ansiStyleCode
  result.add l.fg.ansiForegroundColorCode

proc `$`*(s: StyledLine): string =
  result.add($s.sty)
  result.add(s.str)
  result.add ansiResetCode

proc len*(s: StyledLine): int = s.str.len

proc `$`*(t: StyledText): string =
  for l in t.lines:
    if t.colored:
      result.add $l
    else:
      result.add l.str

    result.add '\n'

proc `&`*(s: StyledLine, t: StyledText): StyledText =
  StyledText(lines: s & t.lines, colored: t.colored)

iterator items*(t: StyledText): StyledLine =
  for l in t.lines:
    yield l

func styleStr*(p: CutePalette | LineStyle, s: string): StyledLine =
  StyledLine(str: s, sty: p)

func styleStr*(style: MdStyle, line: string): StyledLine =
  var str: string
  if line.scanf("#$s$*$.", str):
    result = style.title.styleStr str
  elif line.scanf(">$s$*$.", str):
    discard
    result = style.summary.styleStr str
  elif line.scanf("-$s$*$.", str):
    discard
    result = style.bullet.styleStr str
  elif line.scanf("`$*`", str):
    str = str.multiReplace(("{{", "<"), ("}}", ">"))
    str = str.indent 2
    result = style.code.styleStr str

func styleLines*(sty: StrStyle, lines: seq[string], color = true): StyledText =
  result.colored = color
  for line in lines:
    result.lines.add(sty.styleStr line)

func styleLines*(sty: StrStyle, lines: string, color = true): StyledText =
  sty.styleLines(lines.split '\n', color)

func styleMdText*(lines: string, color = true): StyledText =
  defaultMdSty.styleLines(lines, color)

