import
  std/[
    terminal,
    wordwrap
  ],
  pretty,
  ojea

func stylishWrap*(text: StyledText, maxWidth: int): StyledText =
  result.colored = text.colored
  for l in text:
    if maxWidth < l.len:
      let
        wrap = l.str.wrapWords(maxLineWidth = maxWidth)
        formatted = l.sty.styleLines wrap
      result.lines.add formatted.lines
    else:
      result.lines.add l

proc pagedEcho*(t: StyledText) =
  let lines = t.stylishWrap(terminalWidth())
  pagedEcho($lines)

