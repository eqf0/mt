import
  std/[
    strutils,
    terminal,
  ]

type 
  Keys = object
    down, up, quit, top, bottom: char
  Pager = object
    top: int
    lines: seq[string]
    keys: Keys

proc len(p: Pager): int = p.lines.len

proc `[]`(p: Pager, i: int): string = p.lines[p.top + i]

proc linesPercentage(p: Pager): float =
  ((p.top + terminalHeight() - 1) / p.len).clamp(0, 1)

proc overwriteLineAt(s: string, pos: int) =
  setCursorPos(0, pos)
  eraseLine()
  stdout.write s

proc displayStatus(p: Pager) =
  let 
    percent = $(100 * p.linesPercentage).int & "%"
    down = $p.keys.down & " is down; "
    up = $p.keys.up & " is up; "
    quit = $p.keys.quit & " to quit; "
    tt = $p.keys.top & " to top; "
    bot = $p.keys.bottom & " to bottom; "
    contentLen = down.len + up.len + quit.len + percent.len + tt.len + bot.len
    spaces = ' '.repeat(terminalWidth() - contentLen)
    status = down & up & quit & tt & bot & spaces & percent
  status.overwriteLineAt(terminalHeight() - 1)

proc displayFragment(p: Pager) =
  for i in 0 ..< terminalHeight() - 1:
    var line: string 
    if p.top + i < p.len:
      line = p[i]

    line.overwriteLineAt i
  p.displayStatus()

template withFullscreen(body: untyped) =
  stdout.write "\e[?1049h" # launches alternate buffer
  eraseScreen()
  setCursorPos(0, 0)
  hideCursor()

  body

  stdout.write "\e[?1049l" # restores original buffer
  showCursor()

proc page*(j, k, q, gg, g: static[char], text: seq[string]) =
  var pager = Pager(
    top: 0,
    lines: text,
    keys: Keys(
      down: j,
      up: k,
      quit: q,
      top: gg,
      bottom: g
    )
  )

  withFullScreen:
    pager.displayFragment()
    while true:
      case getCh()
      of q:
        break
      of j:
        if pager.linesPercentage < 1:
          pager.top += 1
          pager.displayFragment()
      of k:
        if 0 < pager.top:
          pager.top -= 1
          pager.displayFragment()
      of g:
        if pager.linesPercentage < 1:
          pager.top = pager.len - terminalHeight()
          pager.displayFragment()
      of gg:
        if 0 < pager.top:
          pager.top = 0
          pager.displayFragment()
      else:
        discard

proc pagedEcho*(s: seq[string]) =
  if terminalHeight() - 1 < s.len:
    page('j', 'k', 'q', 'g', 'G'):
      s
  else:
    echo(s.join "\n")

proc pagedEcho*(s: string) =
  s.split('\n').pagedEcho()
